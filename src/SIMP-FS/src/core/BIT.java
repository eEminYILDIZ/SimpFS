package core;

public class BIT {

	public byte[] array ;

    public BIT() { }

    public BIT(byte[] p_array)
    {
        array = p_array;
    }


    public int Get_Bit(int p_index)
    {
        int bolum = p_index / 8;

        byte data = array[bolum];

        int kalan = p_index % 8;

        // istenilen bit ana veride maskeleniyor:
        byte sayi = 1;
        data &= (byte)(sayi << kalan);

        // maskelenen bit en sağa kaydırılıyor:
        int _return = (int)data;
        _return = _return >> kalan;

        return _return;
    }

    public void Set_Bit(int p_index, int value)
    {
        int bolum = p_index / 8;

        byte data = 1;

        int kalan = p_index % 8;

        data = (byte)(data << kalan);

        if (value == 1)
            array[bolum] |= data;
        else if (value == 0)
            array[bolum] &= (byte)(~data);
    }



    public int Get_First_ZERO_Bit()
    {
        for (int i = 0; i < (array.length * 8); i++)
        {
            if (Get_Bit(i) == 0)
            {
                return i;
            }
        }
        return -1;
    }
	
}
