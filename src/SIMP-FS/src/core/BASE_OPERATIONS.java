package core;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

import objects.DIRECTORY_ENTRY;
import objects.FILE_ENTRY;
import tables.PAGES_FULLOREMPTY_TABLE;

public class BASE_OPERATIONS {

	// for creating and for formatting
	public static void Create() throws IOException
    {
        //c#: FileStream fs = new FileStream(DEFINITIONS.fs_fileName, FileMode.CreateNew);
    	RandomAccessFile fs=new RandomAccessFile(DEFINITIONS.fs_fileName, "rw");
        
        byte[] array = new byte[100];
        for (int i = 0; i < 100; i++)
            array[i] = 0;

        // making a file which have want size:
        for (int i = 0; i < 10000; i++)
            fs.write(array, 0, array.length);

          
        fs.close();

        // for mgfs d_list and mgfs f_list we have to reserve 0. and 1. page table's bits
        PAGES_FULLOREMPTY_TABLE page_table = new PAGES_FULLOREMPTY_TABLE();
        page_table.Reserve_a_Page();
        page_table.Reserve_a_Page();
        
    }

    public static void Formatla() throws IOException
    {
        //c#: FileStream fs = new FileStream(DEFINITIONS.fs_fileName, FileMode.Open);
    	RandomAccessFile fs=new RandomAccessFile(DEFINITIONS.fs_fileName, "rw");
        
        byte[] array = new byte[100];
        for (int i = 0; i < 100; i++)
            array[i] = 0;
        
        // making a file which have want size:
        for (int i = 0; i < 10000; i++)
        	fs.write(array, 0, array.length);
        
        fs.close();
    }

    public static byte[] Oku(byte[] p_array, int p_seek) throws IOException
    {
        //c#: FileStream fs = new FileStream(DEFINITIONS.fs_fileName, FileMode.Open);
    	RandomAccessFile fs=new RandomAccessFile(DEFINITIONS.fs_fileName, "rw");

        fs.seek(p_seek);
        fs.read(p_array, 0, p_array.length);

        fs.close();

        return p_array;
        /*
            Kullanımı:
            byte[] array = new byte[1];
            Oku(ref array, 0);
            Console.WriteLine("0: " + array[0]);
         */
    }

    public static void Yaz(byte[] p_array, int p_seek) throws IOException
    {
        //c#: FileStream fs = new FileStream(DEFINITIONS.fs_fileName, FileMode.Open);
    	RandomAccessFile fs=new RandomAccessFile(DEFINITIONS.fs_fileName, "rw");
    	
        fs.seek(p_seek);
        fs.write(p_array, 0, p_array.length);

        fs.close();
    }


    public static byte[] Directory_Entry_to_ByteArray(DIRECTORY_ENTRY ent)
    {
        byte[] array = new byte[20];

        array[DEFINITIONS.adres_type_relative] = ent.type;

        for (int i = 0; ent.advs[i] != '\0'; i++)
        {
            array[i + DEFINITIONS.adres_advs_relative] = (byte)ent.advs[i];
        }

        byte[] buffer = ByteBuffer.allocate(4).putInt(ent.c_d_list_Adress_relative).array();
	      for (int i = 0; i < 4; i++)
	      {
	          array[DEFINITIONS.adres_cd_list_relative + i] = buffer[i];
	      }
//        for (int i = 0; i < 4; i++)
//        {
//            array[DEFINITIONS.adres_cd_list_relative + i] = (byte)(ent.c_d_list_Adress_relative >> i * 8);
//        }

        byte[] buffer2 = ByteBuffer.allocate(4).putInt(ent.c_f_list_Adress_relative).array();
        for (int i = 0; i < 4; i++)
        {
            array[DEFINITIONS.adres_cf_list_relative + i] = buffer2[i];
        }
//        for (int i = 0; i < 4; i++)
//        {
//            array[DEFINITIONS.adres_cf_list_relative + i] = (byte)(ent.c_f_list_Adress_relative >> i * 8);
//        }

        return array;
    }

    public static DIRECTORY_ENTRY ByteArray_to_Directory_Entry(byte[] array)
    {
        DIRECTORY_ENTRY ent = new DIRECTORY_ENTRY();

        ent.type = array[DEFINITIONS.adres_type_relative];

        for (int i = 0; i < DEFINITIONS.size_advs; i++)
        {
            ent.advs[i] = (char)array[i + DEFINITIONS.adres_advs_relative];

            if (ent.advs[i] == '\0')
                break;
        }

        
        byte[] buffer = new byte[4];
        for (int i = 0; i < 4; i++)
        	buffer[i] = array[DEFINITIONS.adres_cd_list_relative + i];
        ent.c_d_list_Adress_relative = ByteBuffer.wrap(buffer).getInt();
//        for (int i = 0; i < 4; i++)
//        {
//            ent.c_d_list_Adress_relative += (0 + array[DEFINITIONS.adres_cd_list_relative + i]) << i * 8;
//        }

        byte[] buffer2 = new byte[4];
        for (int i = 0; i < 4; i++)
        	buffer2[i] = array[DEFINITIONS.adres_cf_list_relative + i];
        ent.c_f_list_Adress_relative = ByteBuffer.wrap(buffer2).getInt();   
//        for (int i = 0; i < 4; i++)
//        {
//            ent.c_f_list_Adress_relative += (0 + array[DEFINITIONS.adres_cf_list_relative + i]) << i * 8;
//        }

        return ent;
    }


    public static byte[] File_Entry_to_ByteArray(FILE_ENTRY ent)
    {
        byte[] array = new byte[20];

        // TYPE
        array[DEFINITIONS.adres_type_relative] = ent.type;

        // ADVS
        for (int i = 0; ent.advs[i] != '\0'; i++)
        {
            array[i + DEFINITIONS.adres_advs_relative] = (byte)ent.advs[i];
        }

        // DATA LENGTH
        byte[] buffer = ByteBuffer.allocate(4).putInt(ent.data_length).array();
	      for (int i = 0; i < 4; i++)
	      {
	          array[DEFINITIONS.adres_data_length_relative + i] = buffer[i];
	      }
        
//        for (int i = 0; i < 4; i++)
//        {
//            array[DEFINITIONS.adres_data_length_relative + i] = (byte)(ent.data_length >> i * 8);
//        }

        // DATA ADRES:
        byte[] buffer2 = ByteBuffer.allocate(4).putInt(ent.relative_data_beginnig_Adress).array();
        for (int i = 0; i < 4; i++)
        {
            array[DEFINITIONS.adres_data_begginng_relative + i] = buffer2[i];
        }
//        for (int i = 0; i < 4; i++)
//        {
//            array[DEFINITIONS.adres_data_begginng_relative + i] = (byte)(ent.relative_data_beginnig_Adress >> i * 8);
//        }

        return array;
    }

    public static FILE_ENTRY ByteArray_to_File_Entry(byte[] array)
    {
        FILE_ENTRY ent = new FILE_ENTRY();

        ent.type = array[DEFINITIONS.adres_type_relative];

        for (int i = 0; i < DEFINITIONS.size_advs; i++)
        {
            ent.advs[i] = (char)array[i + DEFINITIONS.adres_advs_relative];

            if (ent.advs[i] == '\0')
                break;
        }

//        for (int i = 0; i < 4; i++)
//        {
//        	int will_add = (0 + array[DEFINITIONS.adres_data_length_relative + i]) << i * 8;
//            ent.data_length += will_add;
//        }
        byte[] buffer = new byte[4];
        for (int i = 0; i < 4; i++)
        	buffer[i] = array[DEFINITIONS.adres_data_length_relative + i];
        ent.data_length = ByteBuffer.wrap(buffer).getInt();

//        for (int i = 0; i < 4; i++)
//        {
//            ent.relative_data_beginnig_Adress += (0 + array[DEFINITIONS.adres_data_begginng_relative + i]) << i * 8;
//        }
        byte[] buffer2 = new byte[4];
        for (int i = 0; i < 4; i++)
        	buffer2[i] = array[DEFINITIONS.adres_data_begginng_relative + i];
        ent.relative_data_beginnig_Adress = ByteBuffer.wrap(buffer2).getInt();
        
        return ent;
    }

	
}
