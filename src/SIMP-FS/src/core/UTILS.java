package core;

public class UTILS extends DEFINITIONS{

    //_________________________________________________________________________________________________________________


    public static int Calculate_ListPageNumber_to_Physical_Adres(int p_page_number)
    {
        int _return = 0;
        
        _return += adres_pages_table_of_FuelorEmpty;
        _return += size_pages_table_of_FuelorEmpty + size_data_table_of_FuelorEmpty; // 200 B
        _return += (p_page_number * block_size);

        return _return;
    }

    public static int Calculate_LineNumber_to_Physical_Adres(int p_page_number, int p_line_number)
    {
        int _return = 0;

        _return += adres_pages_table_of_FuelorEmpty;
        _return += size_pages_table_of_FuelorEmpty + size_data_table_of_FuelorEmpty; // 200 B
        _return += (p_page_number * block_size);

        _return += p_line_number * entry_size;

        return _return;
    }

    public static int Calculate_DataPageNumber_to_Physical_Adres(int p_relative_datapage_number)
    {
        int _return = 0;

        _return += adres_data_area; // 400.200 B
        _return += (p_relative_datapage_number * block_size);

        return _return;
    }

    public static char[] String_To_Char(String p_string)
    {
        char[] _return = new char[size_advs];

        for (int i = 0; (i < p_string.length() && i < size_advs); i++)
        {
            _return[i] = p_string.charAt(i);
        }

        _return[p_string.length()] = '\0';

        return _return;
    }

    public static String Char_To_String(char[] p_array)
    {
        String _return = "";

        for (int i = 0; i < p_array.length && p_array[i] != '\0'; i++)
            _return += p_array[i];

        return _return;
    }
	
}
