package core;

public class DEFINITIONS {

    public static String fs_prefix = "#SIMPFS";

    public static String fs_fileName = "simpfs.fs";

    public static int block_size = 1000;// 1K

    public static int adres_pages_table_of_FuelorEmpty = 0;
    // for 50*8 page:
    public static int size_pages_table_of_FuelorEmpty = 100;

    public static int adres_data_table_of_FuelorEmpty = adres_pages_table_of_FuelorEmpty + size_pages_table_of_FuelorEmpty;
    // for 100*8 entry:
    public static int size_data_table_of_FuelorEmpty = 100;

//___________________ENTRIES___________________________________

    // TYPE: 0  = EMPTY LINE
    // TYPE: 1  = DIRETORY
    // TYPE: 2  = FILE

    // sizeof(ENTRY);(byte)
    public static int entry_size = 20;

    public static int adres_type_relative = 0;
    public static int adres_advs_relative = 1;
    public static int adres_cd_list_relative = 12;
    public static int adres_cf_list_relative = 16;

    public static int adres_data_begginng_relative = 16;
    public static int adres_data_length_relative = 12;
    // the name will be max 10 character:
    public static int size_advs = 11;
//______________________________________________________

    public static int max_directory_entry_count = block_size / entry_size;
    public static int max_files_entry_count = block_size / entry_size;

    public static int adres_mgfs_directories_table = 0;
    public static int adres_mgfs_files_table = 1;

    public static int adres_data_area = 400200;// 400.200

    public static int ENDofFILE = 65535; //(FFFF)
//_________________________________________________________________________________________________________________
	
}
