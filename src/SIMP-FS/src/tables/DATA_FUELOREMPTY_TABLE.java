package tables;

import java.io.IOException;

import core.*;

public class DATA_FUELOREMPTY_TABLE {

	 BIT bits = null;
 
     public DATA_FUELOREMPTY_TABLE() throws IOException
     {
         byte[] array = new byte[DEFINITIONS.size_data_table_of_FuelorEmpty];

         array = BASE_OPERATIONS.Oku(array, DEFINITIONS.adres_data_table_of_FuelorEmpty);

         BIT _bits = new BIT();
         _bits.array = array;

         bits = _bits;
     }

     private void Data_Table_Update_to_Disk() throws IOException
     {
         BASE_OPERATIONS.Yaz(bits.array, DEFINITIONS.adres_data_table_of_FuelorEmpty);
     }

     public int Reserve_a_Page() throws IOException
     {
         int index = bits.Get_First_ZERO_Bit();
         bits.Set_Bit(index, 1);

         Data_Table_Update_to_Disk();

         return index;
     }

     public void MakeFree(int p_index) throws IOException
     {
         bits.Set_Bit(p_index, 0);

         Data_Table_Update_to_Disk();
     }
	
}
