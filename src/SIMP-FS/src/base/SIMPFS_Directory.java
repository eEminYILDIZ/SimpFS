package base;

import java.io.IOException;
import java.util.ArrayList;

import core.*;
import objects.*;
import tables.PAGES_FULLOREMPTY_TABLE;

public class SIMPFS_Directory {
	
    public byte[] array_directory_list_byte;
    public byte[] array_file_list_byte;

    public String path ;
    public String parent_path = "";
    

    public DIRECTORY_ENTRY directory_entry;
    public DIRECTORY_ENTRY parent_directory_entry;

    public SIMPFS_Directory current_object = null;

    public SIMPFS_Directory(String p_path) throws IOException{
    	
    	 // if it has / on it's end, delete it:
        if (p_path.endsWith("/"))
            p_path = p_path.substring(0, 0+p_path.length() - 1);

        array_directory_list_byte=new byte[DEFINITIONS.block_size];
       // array_file_list_byte=new byte[DEFINITIONS.block_size];
        
        path = p_path;

        // exp: #MEGAFS/DATA/CONF/PORTS

        String[] directories = p_path.split("/");
        for (int i = 0; i < directories.length; i++)
        {
            if (directories[i].length() == 0)
                continue;

            String searching_directory_name = directories[i];

            if (searching_directory_name.equals(DEFINITIONS.fs_prefix))
            {
            	array_directory_list_byte = BASE_OPERATIONS.Oku(array_directory_list_byte, UTILS.Calculate_ListPageNumber_to_Physical_Adres(DEFINITIONS.adres_mgfs_directories_table));
                DIRECTORY_ENTRY d_entry = new DIRECTORY_ENTRY();
                
                d_entry.type = 1;
                d_entry.advs = UTILS.String_To_Char(DEFINITIONS.fs_prefix);
                d_entry.c_d_list_Adress_relative = DEFINITIONS.adres_mgfs_directories_table;
                d_entry.c_f_list_Adress_relative = DEFINITIONS.adres_mgfs_files_table;
                

                directory_entry = d_entry;
                parent_directory_entry = d_entry;

                parent_path = searching_directory_name;

                current_object = this;
            }
            else
            {
                byte[] line = new byte[DEFINITIONS.entry_size];

                boolean flag = false;

                for (int j = 0; j < DEFINITIONS.max_directory_entry_count; j++)
                {
                    //c#: Array.Copy(array_directory_list_byte, j * 20, line, 0, DEFINITIONS.entry_size);
                    System.arraycopy(array_directory_list_byte, j*20, line, 0, DEFINITIONS.entry_size);
                    
                    if (line[0] == 0)
                        continue;

                    DIRECTORY_ENTRY d_entry = BASE_OPERATIONS.ByteArray_to_Directory_Entry(line);
                    d_entry.directory_list_index = j;

                    if (searching_directory_name.equals(UTILS.Char_To_String(d_entry.advs)))
                    {
                    	array_directory_list_byte = BASE_OPERATIONS.Oku(array_directory_list_byte, UTILS.Calculate_ListPageNumber_to_Physical_Adres(d_entry.c_d_list_Adress_relative));

                        parent_directory_entry = directory_entry;
                        directory_entry = d_entry;

                        parent_path += "/" + searching_directory_name;

                        flag = true;
                        break;
                    }
                }

                if (flag == false)
                {
                    current_object = null;
                    return;
                }

                // sayfalar arası geçişler burada yapılabilir:
            }

            current_object = this;
        }

        if (parent_path.indexOf('/') > 0)
        {
            int indexx = parent_path.lastIndexOf("/");
            parent_path = parent_path.substring(0, 0+indexx);
        }
    	
    }
    
    public boolean isExist()
    {
        if (current_object == null)
            return false;
        else
            return true;
    }

    public ArrayList<DIRECTORY_ENTRY> Get_Directories()
    {
        if (current_object == null)
        {
            //Console.WriteLine("Directory is not Exist");
            return null;
        }


        ArrayList<DIRECTORY_ENTRY> _return = new ArrayList<DIRECTORY_ENTRY>();

        byte[] line = new byte[DEFINITIONS.entry_size];

        for (int j = 0; j < DEFINITIONS.max_directory_entry_count; j++)
        {
            //c#: Array.Copy(array_directory_list_byte, j * 20, line, 0, DEFINITIONS.entry_size);
            System.arraycopy(array_directory_list_byte, j*20, line, 0, DEFINITIONS.entry_size);
            // if line is empty, do not add it the collection.
            if (line[0] == 0)
                continue;

            DIRECTORY_ENTRY d_entry = BASE_OPERATIONS.ByteArray_to_Directory_Entry(line);
            d_entry.directory_list_index = j;

            _return.add(d_entry);
        }

        return _return;
    }
    public ArrayList<FILE_ENTRY> Get_Files() throws IOException
    {
        if (current_object == null)
        {
            //Console.WriteLine("Directory is not Exist");
            return null;
        }

        // if file entry list is not loaded, load it.
        if (array_file_list_byte == null)
        {
            Load_FileEntryList();
        }

        ArrayList<FILE_ENTRY> _return = new ArrayList<FILE_ENTRY>();

        byte[] line = new byte[DEFINITIONS.entry_size];

        for (int j = 0; j < DEFINITIONS.max_files_entry_count; j++)
        {
            //c#: Array.Copy(array_file_list_byte, j * 20, line, 0, DEFINITIONS.entry_size);
            System.arraycopy(array_file_list_byte, j*20, line, 0, DEFINITIONS.entry_size);
            
            // if line is empty, do not add it the collection.
            if (line[0] == 0)
                continue;

            FILE_ENTRY f_entry = BASE_OPERATIONS.ByteArray_to_File_Entry(line);
            f_entry.file_list_index = j;

            _return.add(f_entry);
        }

        return _return;
    }

    private int Find_EMPTY_LINE_FOR_DIRECTORY_ENTRY()
    {
       // byte[] line = new byte[DEFINITIONS.entry_size];

        for (int i = 0; i < DEFINITIONS.max_directory_entry_count; i++)
        {
            
            if (array_directory_list_byte[i * 20] == 0)
                return i;
        }

        return -1;
    }
    public void Load_FileEntryList() throws IOException
    {
        array_file_list_byte = new byte[DEFINITIONS.block_size];
        array_file_list_byte = BASE_OPERATIONS.Oku(array_file_list_byte, UTILS.Calculate_ListPageNumber_to_Physical_Adres(directory_entry.c_f_list_Adress_relative));
    }

    public void Create_Directory(String p_name) throws IOException
    {
        PAGES_FULLOREMPTY_TABLE page_table = new PAGES_FULLOREMPTY_TABLE();

        DIRECTORY_ENTRY ent = new DIRECTORY_ENTRY();
        
        ent.type = 1;
        ent.advs = UTILS.String_To_Char(p_name);
        ent.c_d_list_Adress_relative = page_table.Reserve_a_Page();
        ent. c_f_list_Adress_relative = page_table.Reserve_a_Page();
        

        int write_index = Find_EMPTY_LINE_FOR_DIRECTORY_ENTRY();

        byte[] entry_bytes_array = BASE_OPERATIONS.Directory_Entry_to_ByteArray(ent);

        BASE_OPERATIONS.Yaz(entry_bytes_array, UTILS.Calculate_LineNumber_to_Physical_Adres(directory_entry.c_d_list_Adress_relative, write_index));
    }
    
    public void DeleteDirectory() throws IOException
    {
        if (current_object == null)
        {
            //Console.WriteLine("Directory is not Exist");
            return ;
        }

        ArrayList<DIRECTORY_ENTRY> dir_entries = Get_Directories();
        SIMPFS_File mgfs_file;

        for(DIRECTORY_ENTRY dir_entry : dir_entries)
        {
            String new_path = path + "/" + UTILS.Char_To_String(dir_entry.advs);
            new_path = new_path.replace("//", "/");
            ////////////////////////////////////
            SIMPFS_Directory mdir = new SIMPFS_Directory(new_path);
            mdir.DeleteDirectory();

        }


        // DELETE FILES:
        ArrayList<FILE_ENTRY> file_entries = Get_Files();

        for(FILE_ENTRY fe : file_entries)
        {
            String fe_name = UTILS.Char_To_String(fe.advs);

            mgfs_file = new SIMPFS_File(this, fe_name);

            mgfs_file.DeleteFile();
        }

        //////////////////////////////
        // DELETE CURRENT DIRECTORY //
        //////////////////////////////

        DIRECTORY_ENTRY filled_zeros_d_entry = Take_Filled_Zero_Bytes_Directory_Entry();
        byte[] write_array = BASE_OPERATIONS.Directory_Entry_to_ByteArray(filled_zeros_d_entry);

        PAGES_FULLOREMPTY_TABLE ptb = new PAGES_FULLOREMPTY_TABLE();
        ptb.MakeFree(directory_entry.c_d_list_Adress_relative);
        ptb.MakeFree(directory_entry.c_f_list_Adress_relative);

        BASE_OPERATIONS.Yaz(write_array, UTILS.Calculate_LineNumber_to_Physical_Adres(parent_directory_entry.c_d_list_Adress_relative, directory_entry.directory_list_index));
    }

    private DIRECTORY_ENTRY Take_Filled_Zero_Bytes_Directory_Entry()
    {
        DIRECTORY_ENTRY directory_entry = new DIRECTORY_ENTRY();
        
		directory_entry.advs = UTILS.String_To_Char("");
		directory_entry.c_d_list_Adress_relative = 0;
		directory_entry.c_f_list_Adress_relative = 0;
		directory_entry.type = 0;

        return directory_entry;
    }
    
}
