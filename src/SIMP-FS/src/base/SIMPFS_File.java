package base;

import java.io.IOException;
import java.nio.ByteBuffer;

import core.*;
import objects.FILE_ENTRY;
import tables.DATA_FUELOREMPTY_TABLE;

public class SIMPFS_File {

	
	public SIMPFS_Directory parent_directory = null;
    public FILE_ENTRY file_entry = null;
    String parent_directory_path = "";
    public String file_name = "";

    public SIMPFS_File(String p_path) throws IOException
    {
        String[] splitted_path = p_path.split("/");
        for (int i = 0; i < splitted_path.length; i++)
        {
            if (i == splitted_path.length - 1)
            {
                file_name = splitted_path[i];
                //break;  // not need
            }
            else
            {
                parent_directory_path += splitted_path[i] + "/";
            }
        }

        parent_directory = new SIMPFS_Directory(parent_directory_path);

        // LOAD FILE LIST 
        if (parent_directory.array_file_list_byte == null)
            parent_directory.Load_FileEntryList();
        
        // IF FILE HAVE, LOAD INFORMATION ABOUT IT:
        FILE_ENTRY fentry=LoadFile();
        if(fentry!=null)
        	file_entry = fentry;
    }

    public SIMPFS_File(SIMPFS_Directory p_mgfs_directory,String p_filename)
    {
        parent_directory = p_mgfs_directory;
        file_name = p_filename;
        
        // IF FILE HAVE, LOAD INFORMATION ABOUT IT:
        FILE_ENTRY fentry=LoadFile();
        if(fentry!=null)
        	file_entry = fentry;
    }

    public boolean isExist()
    {
        if (LoadFile() == null)
            return false;
        else
            return true;
    }

    private int Find_EMPTY_LINE_FOR_FILE_ENTRY()
    {
       // byte[] line = new byte[DEFINITIONS.entry_size];

        for (int i = 0; i < DEFINITIONS.max_files_entry_count; i++)
        {

            if (parent_directory.
                array_file_list_byte[i * 20] == 0)
                return i;
        }

        return -1;
    }


    public void Create_File(byte[] p_data) throws IOException
    {
        DATA_FUELOREMPTY_TABLE data_table = new DATA_FUELOREMPTY_TABLE();

        FILE_ENTRY ent = new FILE_ENTRY();
        ent.type = 1;
        ent.advs = UTILS.String_To_Char(file_name);
        ent.relative_data_beginnig_Adress = data_table.Reserve_a_Page();
        ent.data_length = p_data.length;

        int write_index = Find_EMPTY_LINE_FOR_FILE_ENTRY();

        byte[] entry_bytes_array = BASE_OPERATIONS.File_Entry_to_ByteArray(ent);

        // FILE ENTRY WRITING:
        BASE_OPERATIONS.Yaz(entry_bytes_array, UTILS.Calculate_LineNumber_to_Physical_Adres(parent_directory.directory_entry.c_f_list_Adress_relative, write_index));

        // FILE DATA WRITING:
        if (ent.data_length < DEFINITIONS.block_size)
        {
            // FILE DATA:
            BASE_OPERATIONS.Yaz(p_data, UTILS.Calculate_DataPageNumber_to_Physical_Adres(ent.relative_data_beginnig_Adress));
            return;
        }
        else
        {
            byte[] write_array;
            int writed_count = 0;
            int write_adress = ent.relative_data_beginnig_Adress;

            while (writed_count < ent.data_length)
            {

                // 1000 + 1000 + 1000 + 20:
                // for block writing:
                if ((writed_count + DEFINITIONS.block_size) < ent.data_length)
                {
                    write_array = new byte[DEFINITIONS.block_size];

                    // writing a block byte to carrier array:
                    //c#: Array.Copy(p_data, writed_count, write_array, 0, DEFINITIONS.block_size - 4);
                    System.arraycopy(p_data, writed_count, write_array, 0, DEFINITIONS.block_size - 4);
                    
                    // ____TAKING NEXT PAGE ADRESS AND PUTTING IT ON BLOCK BYTE ARRAY'S LAST 4 BYTE.______________
                    int next_relative_page_adress = data_table.Reserve_a_Page();
                    byte[] array_next_relative_page_adress = new byte[4];

//                    for (int i = 0; i < 4; i++)
//                    {
//                        array_next_relative_page_adress[i] = (byte)(next_relative_page_adress >> i * 8);
//                    }
                    array_next_relative_page_adress = ByteBuffer.allocate(4).putInt(next_relative_page_adress).array();

                    // putting last 4 byte
                    //c#: Array.Copy(array_next_relative_page_adress, 0, write_array, DEFINITIONS.block_size - 4, 4);
                    System.arraycopy(array_next_relative_page_adress, 0, write_array, DEFINITIONS.block_size - 4, 4);
                    //____________________________________________________________________________________________


                    // writing a data block:
                    BASE_OPERATIONS.Yaz(write_array, UTILS.Calculate_DataPageNumber_to_Physical_Adres(write_adress));

                    write_adress = next_relative_page_adress;
                    writed_count += DEFINITIONS.block_size - 4;
                }
                // for non block writing
                else
                {
                    int last_size = ent.data_length - writed_count;
                    write_array = new byte[last_size + 4];

                    // writing a block byte to carrier array:
                    //c#: Array.Copy(p_data, writed_count, write_array, 0, last_size);
                    System.arraycopy(p_data, writed_count, write_array, 0, last_size);
                    
                    // for ENDofFILE:
                    int int_endoffile = DEFINITIONS.ENDofFILE;
                    byte[] array_enoffile = new byte[4];
//                    for (int i = 0; i < 4; i++)
//                    {
//                        array_enoffile[i] = (byte)(int_endoffile >> i * 8);
//                    }
                    array_enoffile = ByteBuffer.allocate(4).putInt(int_endoffile).array();
                    
                    //c#: Array.Copy(array_enoffile, 0, write_array, last_size, 4);
                    System.arraycopy(array_enoffile, 0, write_array, last_size, 4);
                    
                    // THIS MADE, BECAUSE: FOR EACH BLOCK WRITING, AFTER ENDOFFILE, BYTES MUST BE ZERO
                    byte[] extended_write_array = new byte[DEFINITIONS.block_size];
                    //c#: Array.Copy(write_array, 0, extended_write_array,0, (last_size + 4));
                    System.arraycopy(write_array, 0, extended_write_array, 0, (last_size+4));
                    
                    writed_count += last_size;

                    // writing a data block:
                    BASE_OPERATIONS.Yaz(extended_write_array, UTILS.Calculate_DataPageNumber_to_Physical_Adres(write_adress));
                }



            }// end of while:
        }// end of else:

    }// end of function

    public byte[] ReadFile() throws IOException
    {

        byte[] line = new byte[DEFINITIONS.entry_size];

        for (int j = 0; j < DEFINITIONS.max_files_entry_count; j++)
        {
            //c#: Array.Copy(parent_directory.array_file_list_byte, j * 20, line, 0, DEFINITIONS.entry_size);
            System.arraycopy(parent_directory.array_file_list_byte, j*20, line, 0, DEFINITIONS.entry_size);
            
            // if line is empty, do not add it the collection.
            if (line[0] == 0)
                continue;

            FILE_ENTRY f_entry = BASE_OPERATIONS.ByteArray_to_File_Entry(line);

            if (UTILS.Char_To_String(f_entry.advs).equals(file_name))
            {
                byte[] _return_data_array = new byte[f_entry.data_length];

                if (f_entry.data_length < DEFINITIONS.block_size)
                {
                	_return_data_array = BASE_OPERATIONS.Oku(_return_data_array, UTILS.Calculate_DataPageNumber_to_Physical_Adres(f_entry.relative_data_beginnig_Adress));
                    return _return_data_array;
                }

                // for 5200 byte // (1000 = block size)
                // 1000 + 1000 + 1000 + 1000 + 1000 + 20    
                else
                {
                    int readed_byte_count = 0;
                    int relative_reading_adres = f_entry.relative_data_beginnig_Adress;

                    while (readed_byte_count < f_entry.data_length)
                    {
                        byte[] readed_block = new byte[DEFINITIONS.block_size];
                        readed_block = BASE_OPERATIONS.Oku( readed_block, UTILS.Calculate_DataPageNumber_to_Physical_Adres(relative_reading_adres));

                        if ((readed_byte_count + DEFINITIONS.block_size) > f_entry.data_length)
                        {
                            int last_read_size = f_entry.data_length - readed_byte_count;
                            System.arraycopy(readed_block, 0, _return_data_array, readed_byte_count, last_read_size);
                            //c#: Array.Copy(readed_block, 0, _return_data_array, readed_byte_count, last_read_size);
                            readed_byte_count += last_read_size;
                            // ....return...
                        }
                        else
                        {
                        	System.arraycopy(readed_block, 0, _return_data_array, readed_byte_count, (DEFINITIONS.block_size - 4));
                            //c#: Array.Copy(readed_block, 0, _return_data_array, readed_byte_count, (DEFINITIONS.block_size - 4));
                            readed_byte_count += DEFINITIONS.block_size - 4;


                            // Finding next Relative Data Page Adress
                            byte[] next_relative_data_page_adres = new byte[4];
                            //c#: Array.Copy(readed_block, readed_block.Length - 4, next_relative_data_page_adres, 0, 4);
                            System.arraycopy(readed_block, readed_block.length - 4, next_relative_data_page_adres, 0, 4);
                            
                            relative_reading_adres = 0;
                            relative_reading_adres = ByteBuffer.wrap(next_relative_data_page_adres).getInt();
//                            for (int i = 0; i < 4; i++)
//                            {
//                                relative_reading_adres += (0 + next_relative_data_page_adres[i]) << i * 8;
//                            }// end of finding
                        }// end of else

                    }// end of while

                    return _return_data_array;
                }// end of else
            }// end of if
        }// end of for

        return null;
    }// end of function

    public FILE_ENTRY LoadFile()
    {
        byte[] line = new byte[DEFINITIONS.entry_size];
        FILE_ENTRY f_entry = new FILE_ENTRY();

        for (int j = 0; j < DEFINITIONS.max_files_entry_count; j++)
        {
            //c#: Array.Copy(parent_directory.array_file_list_byte, j * 20, line, 0, DEFINITIONS.entry_size);
            System.arraycopy(parent_directory.array_file_list_byte, j*20, line, 0, DEFINITIONS.entry_size);
            
            // if line is empty, do not add it the collection.
            if (line[0] == 0)
                continue;

            f_entry = BASE_OPERATIONS.ByteArray_to_File_Entry(line);

            if (UTILS.Char_To_String(f_entry.advs).equals(file_name))
            {
                f_entry.file_list_index = j;
                return f_entry;
            }
        }
        return null;
    }

    private FILE_ENTRY TakeFilledZeroBytes()
    {
        FILE_ENTRY file_entry = new FILE_ENTRY();
        file_entry.advs = UTILS.String_To_Char("");
        file_entry.data_length = 0;
        file_entry.file_list_index = 0;
        file_entry.relative_data_beginnig_Adress = 0;
        
        // this will make that line is empty
        file_entry.type = 0;

        return file_entry;
    }

    public void DeleteFile() throws IOException
    {
        // if direct call deleteFile // not over deleteDirectory
        if (parent_directory == null)
            parent_directory = new SIMPFS_Directory(parent_directory_path);

        // if file entry is null; load file:
        if (file_entry == null)
            file_entry = LoadFile();

        // if file entry is null again; this file is not exist:
        if(file_entry==null)
            ;//Console.WriteLine("File not Exist");

        // DELETE ENTRY FROM PARENT DIRECTORY'S FILE LIST:
        FILE_ENTRY filled_zeros_file_entry = TakeFilledZeroBytes();
        byte[] write_file_entry = BASE_OPERATIONS.File_Entry_to_ByteArray(filled_zeros_file_entry);

        BASE_OPERATIONS.Yaz(write_file_entry, UTILS.Calculate_LineNumber_to_Physical_Adres(parent_directory.directory_entry.c_f_list_Adress_relative, file_entry.file_list_index));

        ///////////////// DELETE ALL BLOCKS FROM PAGE TABLE///////////////////////
        
        byte[] readed_data_array = new byte[file_entry.data_length];

        if (file_entry.data_length < DEFINITIONS.block_size)
        {
            DATA_FUELOREMPTY_TABLE dtb = new DATA_FUELOREMPTY_TABLE();
            dtb.MakeFree(file_entry.relative_data_beginnig_Adress);

            return;
            //BASE_OPERATIONS.Oku(ref _return_data_array, DEFINITIONS.Calculate_DataPageNumber_to_Physical_Adres(file_entry.relative_data_beginnig_Adress));
            //return _return_data_array;
        }

        // for 5200 byte // (1000 = block size)
        // 1000 + 1000 + 1000 + 1000 + 1000 + 20    
        else
        {
            // DELETE ITSELF FROM PAGE TABLE 
            DATA_FUELOREMPTY_TABLE dtb = new DATA_FUELOREMPTY_TABLE();
            dtb.MakeFree(file_entry.relative_data_beginnig_Adress);


            int readed_byte_count = 0;
            int relative_reading_adres = file_entry.relative_data_beginnig_Adress;

            while (readed_byte_count < file_entry.data_length)
            {
                byte[] readed_block = new byte[DEFINITIONS.block_size];
                readed_block = BASE_OPERATIONS.Oku(readed_block, UTILS.Calculate_DataPageNumber_to_Physical_Adres(relative_reading_adres));

                if ((readed_byte_count + DEFINITIONS.block_size) > file_entry.data_length)
                {
                    //int last_read_size = file_entry.data_length - readed_byte_count;
                    //Array.Copy(readed_block, 0, readed_data_array, readed_byte_count, last_read_size);
                    //readed_byte_count += last_read_size;
                    //// ....return...

                    return;
                }
                else
                {
                    //c#: Array.Copy(readed_block, 0, readed_data_array, readed_byte_count, (DEFINITIONS.block_size - 4));
                    System.arraycopy(readed_block, 0, readed_data_array, readed_byte_count, (DEFINITIONS.block_size - 4));
                    readed_byte_count += DEFINITIONS.block_size - 4;

                    // Finding next Relative Data Page Adress
                    byte[] next_relative_data_page_adres = new byte[4];
                    //c#: Array.Copy(readed_block, readed_block.length - 4, next_relative_data_page_adres, 0, 4);
                    System.arraycopy(readed_block, readed_block.length - 4, next_relative_data_page_adres, 0, 4);
                   
                    relative_reading_adres = 0;
                    relative_reading_adres = ByteBuffer.wrap(next_relative_data_page_adres).getInt();
//                    for (int i = 0; i < 4; i++)
//                    {
//                        relative_reading_adres += (0 + next_relative_data_page_adres[i]) << i * 8;
//                    }// end of finding

                    // DELETE NEXT PAGE ADRESS FROM PAGE TABLE 
                    DATA_FUELOREMPTY_TABLE dtb2 = new DATA_FUELOREMPTY_TABLE();
                    dtb2.MakeFree(relative_reading_adres);

                }// end of else

            }// end of while
        }
    }
	
	
}
