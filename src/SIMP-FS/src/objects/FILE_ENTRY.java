package objects;

import core.UTILS;

public class FILE_ENTRY {

    public byte type;
    public char[] advs;
    public int data_length;
    public int relative_data_beginnig_Adress;


    public int file_list_index;

    public FILE_ENTRY()
    {
        type = 0;
        advs = new char[11];
        relative_data_beginnig_Adress = 0;
        data_length = 0;
    }

    @Override
    public String toString()
    {
        return UTILS.Char_To_String(advs);
    }
	
}
