package objects;

import core.UTILS;

public class DIRECTORY_ENTRY {

	 public byte type;
     public char[] advs;
     public int c_d_list_Adress_relative;
     public int c_f_list_Adress_relative;


     public int directory_list_index;

     public DIRECTORY_ENTRY()
     {
         type = 0;
         advs = new char[11];
         c_d_list_Adress_relative = 0;
         c_f_list_Adress_relative = 0;
     }

     @Override
     public String toString()
     {
         return UTILS.Char_To_String(advs);
     }
	
}
