package com.test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;


public class Test_Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	try {
		eqwe();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}
	
	public static void eqwe(){
	
//		int sayi = 47625;
//		
//		byte[] array = new byte[4];
//		
//        for (int i = 0; i < 4; i++)
//        {
//            array[i] = (byte)((sayi >> i * 8) );
//        }
//        
//        int new_sayi = 0;
//        
//        for (int i = 0; i < 4; i++)
//        {
//        	int will_add = (0 + array[i]) << i * 8;
//        	
//            new_sayi += will_add;
//        }
//        
//        System.out.println(new_sayi);
		
	    byte [] bytes = ByteBuffer.allocate(4).putInt(47625).array();
	    
	    System.out.println(ByteBuffer.wrap(bytes).getInt());
		
	}
	
	public static void FTest() throws IOException{
		
		RandomAccessFile raf=new RandomAccessFile("/home/radmin/test.txt", "rw");
		
//		byte[] dizi = "test icerikleri".getBytes();
//		
//		raf.write(dizi);
//		
//		raf.close();
		
		byte[] data =new byte[100]; 
		raf.read(data);
		
		raf.close();
		
		for(byte bt : data)
			System.out.print((char)bt);
		
	}
	
	
	public static void ArrayTest(){
		byte[] dizi = {(byte)5,(byte)4,(byte)3};
		byte[] dizi2 = {(byte)0,(byte)0,(byte)0};
		
		System.arraycopy(dizi, 0, dizi2, 0, dizi.length);
	
	}
	
	public static void RefeA(){
		byte adf = (byte)4;
		
		Refe(adf);
		
		System.out.println(adf);
	
	}
	public static void Refe(byte asd){
		asd = (byte)5;
	}
	
	
	public static void Split_Test(){
		String name = "Asdf/Bafaf/cafef";
		
		String[] pieces = name.split("/");
		
		for (int i = 0; i < pieces.length; i++) {
			System.out.println(pieces[i]);
		}
	}
	
	public static void Split_Test2(){
		String name = "Asdf Bafaf cafef";
		
		String[] pieces = name.split(" ");
		
		for (int i = 0; i < pieces.length; i++) {
			System.out.println(pieces[i]);
		}
	}
	
	public static void Substring_Test(){
		
		String name = "ali_veli";
		
		String kelime=name.substring(4, 4+2);
		
		System.out.println(kelime);
	}
	
	public static void Replace(){
		String name="ali+veli";
		name = name.replace("+", "-");
		
		System.out.println(name);
	}
}
