import java.util.Scanner;

import commands.*;
import core.DEFINITIONS;
import data.DATA_SHARING;

public class SIMPFS_Console {

	public static Scanner system_Console;
	
	public static void main(String[] args)
	{
		system_Console = new Scanner(System.in);
		
		// ANA DOSYA KULLANICIDAN ALINIYOR:
		String file_path="";
		while(!file_path.endsWith("simpfs.fs")){
			System.out.print("FILE PATH: ");
			file_path = system_Console.nextLine();
		}
		
        DEFINITIONS.fs_fileName=file_path;
        DATA_SHARING.Set_WorkingDirectory(DEFINITIONS.fs_prefix);
        
        //Console.Title = DATA_SHARING.Get_WorkingDirectory();

        String readed_line = "";
        String command = "";
        String[] splitted = null;
        String[] _args = null;

        while (true)
        {
        	System.out.print(DATA_SHARING.Get_WorkingDirectory() + ":>");
            readed_line = system_Console.nextLine();

            // for closing console:
            if (readed_line.equals("exit"))
                break;

            // for pressing Enter:
            if (readed_line.length() == 0)
                continue;


            if (readed_line.indexOf(" ") > 0)
            {
                splitted = readed_line.split(" ");

                _args = new String[splitted.length - 1];

                for (int i = 0; i < splitted.length; i++)
                {
                    if (i == 0)
                        command = splitted[0];
                    else
                        _args[i - 1] = splitted[i];

                }

            }
            else
            {
                command = readed_line.trim();
            }



            switch (command.toLowerCase())
            {
                case "cd":
                    CD.Do(_args);
                    break;

                case "pwd":
                    PWD.Do(_args);
                    break;

                case "ls":
                    LS.Do(_args);
                    break;

                case "cat":
                    CAT.Do(_args);
                    break;

                case "rm":
                    RM.Do(_args);
                    break;

                case "mkdir":
                    MKDIR.Do(_args);
                    break;

                case "touch":
                    TOUCH.Do(_args);
                    break;
                    
                case "cp":
                    CP.Do(_args);
                    break;
                    
                case "mv":
                    MV.Do(_args);
                    break;
                    
                case "export":
                    EXPORT.Do(_args);
                    break;
                    
                case "import":
                    IMPORT.Do(_args);
                    break;
                    
                case "info":
                    INFO.Do(_args);
                    break;

                case "format":
                    FORMAT.Do(_args);
                    break;
                    
                case "nano":
                    NANO.Do(_args);
                    break;
                    
                default:
                	System.out.println(">>ERROR>> COMMAND NOT FOUND");
                    break;

            }// case sonu

            
        }// while sonu
        
		
	}// main() sonu

}
