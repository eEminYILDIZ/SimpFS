package commands;

import java.io.IOException;

import base.SIMPFS_Directory;
import core.DEFINITIONS;
import data.DATA_SHARING;

public class CD {
	
	   public static void Do(String[] p_args)
       {
           switch (p_args[0])
           {
               case "\\":
                   CD_BACKSLASH();
                   break;
               case "..":
                   CD_DOUBLEDOT();
                   break;
               default:
                   CD_Path(p_args[0]);
                   break;
           }
       }

       private static void CD_Path(String p_path)
       {
    	   try{
       
	           if (p_path.startsWith(DEFINITIONS.fs_prefix))
	           {
	               SIMPFS_Directory mg_dir = new SIMPFS_Directory(p_path);
	               if (mg_dir.isExist())
	                   DATA_SHARING.Set_WorkingDirectory(p_path);
	           }
	           else
	           {
	               String current_WD = DATA_SHARING.Get_WorkingDirectory();
	
	               if (current_WD.endsWith("/"))
	                   current_WD = current_WD + p_path;
	               else
	                   current_WD = current_WD + "/" + p_path;
	
	               SIMPFS_Directory mg_dir = new SIMPFS_Directory(current_WD);
	               if (mg_dir.isExist())
	                   DATA_SHARING.Set_WorkingDirectory(current_WD);
	           }
	           
    	   }catch(Exception ex){
    		   System.out.println(ex.getMessage());
    	   }
       }

       private static void CD_BACKSLASH()
       {
           DATA_SHARING.Set_WorkingDirectory(DEFINITIONS.fs_prefix);
       }

       private static void CD_DOUBLEDOT()
       {
    	   try{
    		   
	           String new_WD = DATA_SHARING.Get_WorkingDirectory();
	
	           if (new_WD.indexOf("/") > 0)
	           {
	               int index = DATA_SHARING.Get_WorkingDirectory().lastIndexOf('/');
	               new_WD = DATA_SHARING.Get_WorkingDirectory().substring(0, 0+index);
	           }
	
	           SIMPFS_Directory mg_dir = new SIMPFS_Directory(new_WD);
	           if (mg_dir.isExist())
	               DATA_SHARING.Set_WorkingDirectory(new_WD);
	           
    	   }catch(IOException ex){
    		   System.out.println(ex.getMessage());
    	   }
       }
}
