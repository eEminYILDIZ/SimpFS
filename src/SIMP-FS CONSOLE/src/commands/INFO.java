package commands;

import java.io.IOException;

import base.SIMPFS_File;
import core.DEFINITIONS;
import data.DATA_SHARING;

public class INFO {
	 public static void Do(String[] _args)
     {
		 INFO_GENERAL(_args[0]);
     }

     public static void INFO_GENERAL(String p_filename)
     {
   	  try {
	    	  
	          String file_path = "";
	
	          if (p_filename.startsWith(DEFINITIONS.fs_prefix))
	          {
	              file_path = p_filename;
	          }
	          else
	          {
	              String curr_WD = DATA_SHARING.Get_WorkingDirectory();
	              if (curr_WD.endsWith("/"))
	                  file_path = curr_WD + p_filename;
	              else
	                  file_path = curr_WD + "/" + p_filename;
	          }
	
	          SIMPFS_File mg_file;
			
	          mg_file = new SIMPFS_File(file_path);
		
	          if(!mg_file.isExist())
        	  {
	        	  System.out.println("ERROR: THIS IS NOT A FILE");
        	  }
	          else
	          {
		          System.out.println("FILE NAME:\t"+mg_file.file_name);
		          System.out.println("FILE SIZE:\t"+mg_file.file_entry.data_length+" Bytes");
		          System.out.println("FILE PATH:\t"+mg_file.parent_directory.path);
	          }        
	          //System.out.println("\n");
   
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			System.out.println(ex.getMessage());
		}
     }
}
