package commands;

import base.SIMPFS_Directory;
import base.SIMPFS_File;
import core.DEFINITIONS;
import data.DATA_SHARING;

public class RM {
	 public static void Do(String[] _args)
     {
         RM_GENERAL(_args[0]);
     }

     public static void RM_GENERAL(String p_filename)
     {
    	 try{
    		    	 
	         String file_path = "";
	
	         if (p_filename.startsWith(DEFINITIONS.fs_prefix))
	         {
	             file_path = p_filename;
	         }
	         else
	         {
	             String curr_WD = DATA_SHARING.Get_WorkingDirectory();
	             if (curr_WD.endsWith("/"))
	                 file_path = curr_WD + p_filename;
	             else
	                 file_path = curr_WD + "/" + p_filename;
	         }
	
	         SIMPFS_Directory mg_dir = new SIMPFS_Directory(file_path);
	         if (mg_dir.isExist()) { 
	             mg_dir.DeleteDirectory();
	             return;
	         }
	
	         SIMPFS_File mg_file = new SIMPFS_File(file_path);
	         if (mg_file.isExist())
	         {
	             mg_file.DeleteFile();
	             return;
	         }
	
	         System.out.println("ERROR: THIS IS NOT A FILE OR DIRECTORY...");
	    	 
    	 }catch(Exception ex){
    		 System.out.println(ex.getMessage());
    	 }
     }
}
