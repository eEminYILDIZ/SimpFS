package commands;

import java.util.ArrayList;

import base.SIMPFS_Directory;
import core.UTILS;
import data.DATA_SHARING;
import objects.DIRECTORY_ENTRY;
import objects.FILE_ENTRY;

public class LS {

    public static void Do(String[] _args)
    {
        LS_GENERAL();
    }

     public static void LS_GENERAL()
     {
    	 try{
    	 
         SIMPFS_Directory mg_dir = new SIMPFS_Directory(DATA_SHARING.Get_WorkingDirectory());

         ArrayList<DIRECTORY_ENTRY> dir_entries = mg_dir.Get_Directories();
         ArrayList<FILE_ENTRY> file_entries = mg_dir.Get_Files();

         for (DIRECTORY_ENTRY dir_entry : dir_entries)
        	 System.out.println("DIR \t" + UTILS.Char_To_String(dir_entry.advs));
         
         for (FILE_ENTRY file_entry : file_entries)
             System.out.println("FILE\t" + UTILS.Char_To_String(file_entry.advs));
         
    	 }catch(Exception ex){
    		 System.out.println(ex.getMessage());
    	 }
     }
     
}
