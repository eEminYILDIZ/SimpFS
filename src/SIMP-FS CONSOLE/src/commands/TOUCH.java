package commands;

import base.SIMPFS_File;
import data.DATA_SHARING;

public class TOUCH {
	 
	public static void Do(String[] _args)
     {
         TOUCH_GENERAL(_args[0]);
     }

     public static void TOUCH_GENERAL(String p_filename)
     {
    	 try{
    		 
	         String file_path = "";
	         String curr_WD = DATA_SHARING.Get_WorkingDirectory();
	         if (curr_WD.endsWith("/"))
	             file_path = curr_WD + p_filename;
	         else
	             file_path = curr_WD + "/" + p_filename;
	
	         SIMPFS_File mg_file = new SIMPFS_File(file_path);
	         if (mg_file.isExist())
	         {
	        	 System.out.println(">>ERROR: THERE IS A FILE THE SAME NAME AS THIS");
	             return;
	         }
	         else
	         {
	             mg_file.Create_File(new byte[2]);
	         }
    	 }catch(Exception ex){
    		 System.out.println(ex.getMessage());
    	 }
     }
}
