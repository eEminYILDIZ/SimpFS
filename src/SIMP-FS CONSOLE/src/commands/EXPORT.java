package commands;

import java.io.IOException;
import java.io.RandomAccessFile;

import base.SIMPFS_File;
import core.DEFINITIONS;
import data.DATA_SHARING;

public class EXPORT {
	public static void Do(String[] _args)
    {
		EXPORT_GENERAL(_args[0],_args[1]);
    }

    public static void EXPORT_GENERAL(String p_source_filename,String p_destination_filename)
    {
  	  try {
	
// READING SOURCE FILE:
//________________________________________________________________________________________
  		  
	          String source_file_path = "";
	
	          if (p_source_filename.startsWith(DEFINITIONS.fs_prefix))
	          {
	              source_file_path = p_source_filename;
	          }
	          else
	          {
	              String curr_WD = DATA_SHARING.Get_WorkingDirectory();
	              if (curr_WD.endsWith("/"))
	                  source_file_path = curr_WD + p_source_filename;
	              else
	                  source_file_path = curr_WD + "/" + p_source_filename;
	          }
	
	          SIMPFS_File source_file = new SIMPFS_File(source_file_path);
		
	          if(!source_file.isExist())
	        	  System.out.println("ERROR: SOURCE IS NOT A FILE");
	              
	
	          byte[] source_data = source_file.ReadFile();
	          if (source_data == null)
	          {
	        	  System.out.println("ERROR: SOURCE FILE CAN NOT READED...");
	          }
	          
	      
// WRITING TO DESTINATION FILE:
//________________________________________________________________________________________  
	          
	          RandomAccessFile destination_file=new RandomAccessFile(p_destination_filename, "rw");
	
	          // CREATING DESTINATION FILE:
	          destination_file.write(source_data);
	          
	          destination_file.close();
	
	
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			System.out.println(ex.getMessage());
		}
    }
}
