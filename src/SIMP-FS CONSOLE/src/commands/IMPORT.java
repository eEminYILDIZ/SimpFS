package commands;

import java.io.IOException;
import java.io.RandomAccessFile;

import base.SIMPFS_File;
import core.DEFINITIONS;
import data.DATA_SHARING;

public class IMPORT {
	public static void Do(String[] _args)
    {
		IMPORT_GENERAL(_args[0],_args[1]);
    }

    public static void IMPORT_GENERAL(String p_source_filename,String p_destination_filename)
    {
  	  try {
	
// READING SOURCE FILE:
//________________________________________________________________________________________
  		  
  		  RandomAccessFile source_file = new RandomAccessFile(p_source_filename, "r");
  		  
          byte[] source_data = new byte[ (int)source_file.length()];
          
          source_file.read(source_data);
          
          source_file.close();
          
	      
// WRITING TO DESTINATION FILE:
//________________________________________________________________________________________  
	          
          String destination_file_path = "";
      	
          if (p_destination_filename.startsWith(DEFINITIONS.fs_prefix))
          {
        	  destination_file_path = p_destination_filename;
          }
          else
          {
              String curr_WD = DATA_SHARING.Get_WorkingDirectory();
              if (curr_WD.endsWith("/"))
            	  destination_file_path = curr_WD + p_destination_filename;
              else
            	  destination_file_path = curr_WD + "/" + p_destination_filename;
          }

          SIMPFS_File destination_file = new SIMPFS_File(destination_file_path);
	
          if(destination_file.isExist())
        	  System.out.println("ERROR: THERE IS ALREADY DESTINATION FILE");
              

          destination_file.Create_File(source_data);
	
  
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			System.out.println(ex.getMessage());
		}
    }
}
