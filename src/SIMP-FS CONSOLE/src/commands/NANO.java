package commands;

import java.io.IOException;
import java.util.Scanner;

import base.SIMPFS_File;
import core.DEFINITIONS;
import data.DATA_SHARING;

public class NANO {
	public static void Do(String[] _args)
    {
		NANO_GENERAL(_args[0]);
    }

    public static void NANO_GENERAL(String p_destination_filename)
    {
  	  try {
	
// READING SOURCE FILE:
//________________________________________________________________________________________
  		 String will_write="";
  		  
  		 Scanner scan = new Scanner(System.in);
  		 String satir="";
  		 
  		 while(true){
  			satir = scan.nextLine();
  			
  			if(!satir.equals("exit nano")) 
  				will_write += satir+"\n";
  			else
  				break;
  		 }
         
         
  		 byte[] source_data = will_write.getBytes();
	      
// WRITING TO DESTINATION FILE:
//________________________________________________________________________________________  
	          
          String destination_file_path = "";
      	
          if (p_destination_filename.startsWith(DEFINITIONS.fs_prefix))
          {
        	  destination_file_path = p_destination_filename;
          }
          else
          {
              String curr_WD = DATA_SHARING.Get_WorkingDirectory();
              if (curr_WD.endsWith("/"))
            	  destination_file_path = curr_WD + p_destination_filename;
              else
            	  destination_file_path = curr_WD + "/" + p_destination_filename;
          }

          SIMPFS_File destination_file = new SIMPFS_File(destination_file_path);
	
          if(destination_file.isExist())
        	  System.out.println("ERROR: THERE IS ALREADY DESTINATION FILE");
              

          destination_file.Create_File(source_data);
	
  
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			System.out.println(ex.getMessage());
		}
    }
}
