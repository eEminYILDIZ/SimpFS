package commands;

import java.io.IOException;

import base.SIMPFS_File;
import core.DEFINITIONS;
import data.DATA_SHARING;

public class CAT {
	  public static void Do(String[] _args)
      {
          CAT_GENERAL(_args[0]);
      }

      public static void CAT_GENERAL(String p_filename)
      {
    	  try {
	    	  
	          String file_path = "";
	
	          if (p_filename.startsWith(DEFINITIONS.fs_prefix))
	          {
	              file_path = p_filename;
	          }
	          else
	          {
	              String curr_WD = DATA_SHARING.Get_WorkingDirectory();
	              if (curr_WD.endsWith("/"))
	                  file_path = curr_WD + p_filename;
	              else
	                  file_path = curr_WD + "/" + p_filename;
	          }
	
	          SIMPFS_File mg_file;
			
				mg_file = new SIMPFS_File(file_path);
		
	          if(!mg_file.isExist())
	        	  System.out.println("ERROR: THIS IS NOT A FILE");
	              
	
	          byte[] data = mg_file.ReadFile();
	          if (data == null)
	        	  System.out.println("ERROR: FILE CAN NOT READED...");
	              
	          else
	              for (byte biyt : data)
	              	System.out.print((char)biyt);
	                  
	
	          System.out.println("");
    
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			System.out.println(ex.getMessage());
		}
      }
}
