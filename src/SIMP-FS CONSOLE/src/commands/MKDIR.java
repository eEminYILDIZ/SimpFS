package commands;

import base.SIMPFS_Directory;
import data.DATA_SHARING;

public class MKDIR {
	   
	public static void Do(String[] _args)
       {
           MKDIR_GENERAL(_args[0]);
       }

       public static void MKDIR_GENERAL(String p_filename)
       {
    	   try{
    		   
	           String dir_path = "";
	           String curr_WD = DATA_SHARING.Get_WorkingDirectory();
	           if (curr_WD.endsWith("/"))
	               dir_path = curr_WD + p_filename;
	           else
	               dir_path = curr_WD + "/" + p_filename;
	
	           SIMPFS_Directory mg_dir = new SIMPFS_Directory(dir_path);
	           if (mg_dir.isExist())
	           {
	        	   System.out.println(">>ERROR: THERE IS A DIRECTORY THE SAME NAME AS THIS");
	               return;
	           }
	           else
	           {
	               SIMPFS_Directory c_mg_dir = new SIMPFS_Directory(curr_WD);
	               c_mg_dir.Create_Directory(p_filename);
	           }
    	   }catch(Exception ex){
    		   System.out.println(ex.getMessage());
    	   }
       }
}
